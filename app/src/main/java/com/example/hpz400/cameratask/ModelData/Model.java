package com.example.hpz400.cameratask.ModelData;

/**
 * Created by Hp Z400 on 3/21/2018.
 */

public class Model {

    String name;
    String ReceivingLocation;


    public  Model (){

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReceivingLocation() {
        return ReceivingLocation;
    }

    public void setReceivingLocation(String receivingLocation) {
        ReceivingLocation = receivingLocation;
    }
}
