package com.example.hpz400.cameratask;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.example.hpz400.cameratask.Adapter.Adapter;
import com.example.hpz400.cameratask.ModelData.Model;
import com.example.hpz400.cameratask.SqliteDataBase.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyListView extends AppCompatActivity {


    DatabaseHelper databaseHelper;
    Cursor cursor;
    Adapter adapter;

   ArrayList<Model> Mydata;
    ListView listView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        listView = (ListView) findViewById(R.id.mylist);

        databaseHelper = new DatabaseHelper(this);
        Mydata = new ArrayList<>();


        List<Model> res = databaseHelper.getAllData();
        if(res.size() == 0){
//            showMessage("Error","Nothing found");
            return;
        }else {
            for (int i = 0; i<res.size(); i++){
                String Name = res.get(i).getName();
                String Location = res.get(i).getReceivingLocation();

                Model model = new Model();
                model.setName(Name);
                model.setReceivingLocation(Location);

                Mydata.add(model);
                adapter = new Adapter(MyListView.this,Mydata);

                listView.setAdapter(adapter);

            }
        }






/*            Mydata = new ArrayList<>();

            Mydata.get().getName()*/




        }
    }

 