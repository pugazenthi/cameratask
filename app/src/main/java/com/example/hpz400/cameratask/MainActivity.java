package com.example.hpz400.cameratask;

import android.Manifest;
import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "";
    Button button;
    ImageView imageView;
    Intent intent;
    Bitmap bitmap;

    public static final int RequestPermissionCoding =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        imageView = (ImageView) findViewById(R.id.ImageView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 7);

            }
        });

     }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==7&& resultCode== Activity.RESULT_OK){

            bitmap=(Bitmap) data.getExtras().get("data");
            File PictureDir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            File dir=new File (PictureDir+"/save/");
            dir.mkdir();
            String PicName=getPictureName();
            File ImageFile=new File(dir,PicName);
            try{
                OutputStream out = null;
                out =new FileOutputStream(ImageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,out);
                out.flush();
                out.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Uri uri=Uri.parse(ImageFile.getAbsolutePath());
            imageView.setImageURI(uri);
            String Path= uri.getPath();

            Intent intent = new Intent(this,ImageDataDisplay.class);
            intent.putExtra("Image",Path);
            startActivity(intent);
        }
}

    private String getPictureName() {
        return "PlazePlaceImage" + bitmap + ".jpg";
    }

}



        // 1. this is the method for before accessing the camera to get permission from user
      /*  EnableRuntimepermission();*/
        //2. this will ne open the camera while clicking on it
       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 7);
                Bitmap bitmap= BitmapFactory.decodeResource(getResources(),R.drawable.user);
                imageView.setImageBitmap(bitmap);

                File PictureDir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                File dir=new File (PictureDir+"/save/");
                dir.mkdir();
                String PicName=getPictureName();
                File ImageFile=new File(dir,PicName);

                if(ImageFile==null);
                {
                    Log.d(TAG,"error");
                    return;
                }
                try{
                    FileOutputStream fos=new FileOutputStream(ImageFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                    fos.close();



                } catch (FileNotFoundException e) {
                    Log.d(TAG, "File not found: " + e.getMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d(TAG, "Error accessing file: " + e.getMessage());
                    e.printStackTrace();
                }


                Uri uri= Uri.fromFile(ImageFile);
                Log.i(TAG, "savedImageURL " + uri);

                Intent intent = new Intent(this,MainActivity.class);
                intent.putExtra("Image",uri);
                startActivity(intent);


            }
        });*/

    //3. this was the frist method checking the access result from  user in to REQUESTCODE PERSMISSION(Num) and set the image into imageview
  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==7&& resultCode== Activity.RESULT_OK){





        }
    }*/

  /*  private String getPictureName() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYDDMM");
        String Timestamp = simpleDateFormat.format(new Date());
        return "PlazePlaceImage" + Timestamp + ".jpg";
    }*/

    // 4. THIS IS THE second method to checking the request was (first param request code passed )(secound param Never Null)(third param granted or not)
/*    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {
            case RequestPermissionCoding:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                } else {
                   Toast.makeText(MainActivity.this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }*/

/*

    private void EnableRuntimepermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.CAMERA))
        {
           Toast.makeText(MainActivity.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA}, RequestPermissionCoding);



        }
    }
*/





