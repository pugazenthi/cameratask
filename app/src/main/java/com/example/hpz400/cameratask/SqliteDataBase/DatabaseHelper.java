package com.example.hpz400.cameratask.SqliteDataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.hpz400.cameratask.ModelData.Model;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hp Z400 on 3/21/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "images";
    public static final String TABLE_NAME = "imagetext";
    public static final String COL_0 = "ID";
    public static final String COL_1 = "NAME";
    public static final String COL_2 = "RecivingLocation";
    private static final String LOG ="" ;

    //sql database asign

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,1);
        Log.d(LOG,"Created");


    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+TABLE_NAME+"(ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT,RecivingLocation TEXT)");
        Log.d(LOG,"table created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);

    }


    public boolean insertData(String name,String RecivingLocation) {
        ArrayList<Model> array_list = new ArrayList<Model>();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,name);
        contentValues.put(COL_2,RecivingLocation);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public List<Model> getAllData() {
        List<Model> array_list = new ArrayList<Model>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from imagetext",null);
        if (res.moveToFirst()){
            do{
                Model model = new Model();
                model.setName(res.getString(res.getColumnIndex(COL_1)));
                model.setReceivingLocation(res.getString(res.getColumnIndex(COL_2)));
//                res.getString(res.getColumnIndex("COL_2"));

                array_list.add(model);

           }while(res.moveToNext());
        }
        res.close();

        return array_list;
    }
}
