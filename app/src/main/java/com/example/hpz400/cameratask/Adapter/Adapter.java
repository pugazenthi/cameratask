package com.example.hpz400.cameratask.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hpz400.cameratask.ModelData.Model;
import com.example.hpz400.cameratask.MyListView;
import com.example.hpz400.cameratask.R;

import java.util.ArrayList;

/**
 * Created by Hp Z400 on 3/21/2018.
 */

public class Adapter extends BaseAdapter {
 Context ctx;
 ArrayList<Model> models;


    public Adapter(Context ctx, ArrayList<Model> models) {
        this.ctx = ctx;
        this.models = models;
    }



    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Model list= models.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.card_view, null);

        }
         TextView  name = (TextView) convertView.findViewById(R.id.text);
         name.setText(list.getName());
         ImageView images=(ImageView)convertView.findViewById(R.id.images);
        Bitmap bitmap= BitmapFactory.decodeFile(list.getReceivingLocation());
         images.setImageBitmap(bitmap);
         return convertView;

        }
    }

