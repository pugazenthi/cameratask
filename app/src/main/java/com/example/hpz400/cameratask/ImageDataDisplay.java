package com.example.hpz400.cameratask;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hpz400.cameratask.SqliteDataBase.DatabaseHelper;

import java.util.List;

public class ImageDataDisplay extends AppCompatActivity {
    private static final String TAG = "";
    EditText text;
    Button viewall, Save,ListAll;
    Intent intent;
    Bitmap bitmap;
    ImageView viewBitmap;
    String RecivingLocation;

    DatabaseHelper myDb;


/*
//Folder path for Firebase Storage.
    String Storage_Path = "All_Image_Uploads/";

    // Root Database Name for Firebase Database.
    String Database_Path = "All_Image_Uploads_Database";

    StorageReference storageReference;
    DatabaseReference databaseReference;
    ProgressDialog progressDialog ;
*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_data_display);

        viewall=(Button)findViewById(R.id.viewall);
        ListAll=(Button)findViewById(R.id.ListAll);
        ListAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ImageDataDisplay.this,MyListView.class);
                startActivity(intent);
            }
        });


        Toast.makeText(this, "image came", Toast.LENGTH_SHORT).show();
        RecivingLocation  = getIntent().getExtras().getString("Image");
        Bitmap bitmap= BitmapFactory.decodeFile(RecivingLocation);
        viewBitmap = (ImageView)findViewById(R.id.ImageView);
        viewBitmap.setImageBitmap(bitmap);
        text=(EditText)findViewById(R.id.Text);
        myDb = new DatabaseHelper(this);

        AddData();
        viewAll();


    }

    private void viewAll() {
        viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Cursor res = myDb.getAllData();
//                if(res.getCount()==0){
//                    showMessage("Error","Nothing found");
//                    return;
//                }
//                StringBuffer buffer = new StringBuffer();
//                while (res.moveToNext())  {
//                    buffer.append("NAME:"+res.getString(0)+"\n");
//                    buffer.append("RecivingLocation:"+res.getString(1));
//                }
//                showMessage("datas",buffer.toString());
            }
        });
    }


    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    private void AddData() {
        Save = (Button) findViewById(R.id.Save);
        Save.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    boolean insert =  myDb.insertData(text.getText().toString(),RecivingLocation);
                                    if(insert) {
                                        Toast.makeText(ImageDataDisplay.this, "insert", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(ImageDataDisplay.this, "not insert", Toast.LENGTH_SHORT).show();

                                        }
                                    }
                                }

        );


    }


}



/*        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference(Database_Path);*/

        //get the image from data




        /*Submit = (Button) findViewById(R.id.uploadImg);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = ((BitmapDrawable) viewBitmap.getDrawable()).getBitmap();
                //always save as
                String fileName = "test.jpg";

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

                File ExternalStorageDirectory = Environment.getExternalStorageDirectory();
                File file = new File(ExternalStorageDirectory + File.separator + fileName);
//1.
                FileOutputStream fileOutputStream = null;
                try {
                    file.createNewFile();
                    fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(bytes.toByteArray());

                    Toast.makeText(ImageDataDisplay.this,
                            file.getAbsolutePath(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {

                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }


        });
*/


 /*        2.       viewBitmap.setDrawingCacheEnabled(true);
                Bitmap bitmap = viewBitmap.getDrawingCache();

                String root = Environment.getExternalStorageDirectory().toString();
                File newDir = new File(root + "/saved_images");
                newDir.mkdirs();
                Random gen = new Random();
                int n = 10000;
                n = gen.nextInt(n);
                String fotoname = "photo-"+ n +".jpg";
                File file = new File (newDir, fotoname);
                if (file.exists ()) file.delete ();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                    Toast.makeText(getApplicationContext(), "safed to your folder", Toast.LENGTH_SHORT ).show();

                } catch (Exception e) {

                }*/


/*3.    private boolean storeImage(Bitmap imageData, String filename) {
        //get path to external storage (SD card)
        String iconsStoragePath = Environment.getExternalStorageDirectory() + "/myAppDir/myImages/";
        File sdIconStorageDir = new File(iconsStoragePath);

        //create storage directories, if they don't exist
        sdIconStorageDir.mkdirs();

        try {
            String filePath = sdIconStorageDir.toString() + filename;
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            //choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.PNG, 100, bos);
            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        return true;
    }*/







        /*4.  OutputStream fOut = null;
        Uri outputFileUri;
        try {
            File root = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "folder_name" + File.separator);
            root.mkdirs();
            File sdImageMainDirectory = new File(root, "myPicName.jpg");
            outputFileUri = Uri.fromFile(sdImageMainDirectory);
            fOut = new FileOutputStream(sdImageMainDirectory);

        } catch (Exception e) {

            Toast.makeText(this, "Error occured. Please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
        try {

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {      }*/


